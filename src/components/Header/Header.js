import React from 'react';
import Bio from '../Bio/Bio'

const Header = () => {
    return (
        <div>
            <Bio
                name="Дмитро"
                lastname="Сорока"
            />  
        </div>
    )
}

export default Header;